<?php

namespace Tests\AppBundle\Controller;

use Coduo\PHPMatcher\Matcher;
use Lakion\ApiTestCase\JsonApiTestCase;

class PasteControllerTest extends JsonApiTestCase
{
    public function testIndex()
    {
        $this->client->request('GET', '/paste/');

        $content = $this->client->getResponse()->getContent();
        $decoded = json_decode($content);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertInternalType('array', $decoded);
    }

    public function Should_ReturnId_When_PasteCreated()
    {
        $this->client->request('POST', '/paste/', [], [], [],
            json_encode([
                'content' => 'test content'
            ]));

        $res = $this->client->getResponse();

        $data = json_decode($res->getContent());
        $this->assertArrayHasKey('id', $data);
    }
}
