<?php

namespace AuthBundle\Listener;


use AuthBundle\Exception\ValidationFailedHttpException;
use JMS\Serializer\Exception\ValidationFailedException;
use JMS\Serializer\GraphNavigator;
use JMS\Serializer\Handler\SubscribingHandlerInterface;
use JMS\Serializer\JsonSerializationVisitor;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Validator\ConstraintViolationInterface;

class ValidationSerializationListener implements SubscribingHandlerInterface
{
    /**
     * @return array
     */
    public static function getSubscribingMethods()
    {
        return [
            [
                'direction' => GraphNavigator::DIRECTION_SERIALIZATION,
                'format' => 'json',
                'type' => 'AuthBundle\Exception\ValidationFailedHttpException',
                'method' => 'serialize',
            ],
        ];
    }

    /**
     * @param JsonSerializationVisitor $visitor
     * @param ValidationFailedHttpException|ValidationFailedException $exception
     * @param array $type
     * @return \ArrayObject
     */
    public function serialize(
        JsonSerializationVisitor $visitor,
        ValidationFailedHttpException $exception,
        array $type
    ) {
        $isRoot = null === $visitor->getRoot();

        $accessor = PropertyAccess::createPropertyAccessor();
        $form = new \ArrayObject();
        $errors = [];
        $violations = $exception->getConstraintViolationList();

        foreach ($violations as $violation) {
            /** @var ConstraintViolationInterface $violation */
            $accessor->setValue($errors, $violation->getPropertyPath(), $violation->getMessage());
        }

        $form['errors'] = $errors;

        if ($isRoot) {
            $visitor->setRoot($form);
        }

        return $form;
    }
}