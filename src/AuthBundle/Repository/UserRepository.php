<?php

namespace AuthBundle\Repository;

class UserRepository extends \Doctrine\ORM\EntityRepository
{
    public function findOneByUsername($username)
    {
        return $this->findOneBy([
            'username' => $username
        ]);
    }
}
