<?php

namespace AuthBundle\Repository;

class UserTokenRepository extends \Doctrine\ORM\EntityRepository
{
    public function findOneByToken($token)
    {
        return $this->findOneBy(['token' => $token]);
    }
}
