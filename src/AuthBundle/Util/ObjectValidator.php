<?php

namespace AuthBundle\Util;

use AuthBundle\Exception\ValidationFailedHttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ObjectValidator
{
    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * ObjectValidator constructor.
     * @param ValidatorInterface $validator
     */
    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @param $object
     * @param $rules
     */
    public function validate($object, $rules)
    {
        $errors = $this->validator->validate($object, $rules);

        if (0 !== count($errors)) {
            throw new ValidationFailedHttpException($errors);
        }
    }
}
