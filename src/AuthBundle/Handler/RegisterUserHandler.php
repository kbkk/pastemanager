<?php

namespace AuthBundle\Handler;


use AuthBundle\Command\RegisterUserCommand;
use AuthBundle\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegisterUserHandler
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    public function __construct(EntityManagerInterface $em, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->em = $em;
        $this->passwordEncoder = $passwordEncoder;
    }

    public function handle(RegisterUserCommand $command)
    {
        $user = new User();
        $user->setUsername($command->username);
        $user->setEmail($command->email);

        $encodedPassword = $this->passwordEncoder->encodePassword($user, $command->password);
        $user->setPassword($encodedPassword);

        $this->em->persist($user);
        $this->em->flush();

        return $user;
    }
}