<?php

namespace AuthBundle\Command;

class RegisterUserCommand
{
    public $username;
    public $email;
    public $password;

    public function __construct($username, $email, $password)
    {
        if ($username === null) {
            throw new \DomainException('Missing parameter "username"');
        }

        if ($email === null) {
            throw new \DomainException('Missing parameter "email"');
        }

        if ($password === null) {
            throw new \DomainException('Missing parameter "password"');
        }

        $this->username = $username;
        $this->email = $email;
        $this->password = $password;
    }
}
