<?php

namespace AuthBundle\Controller;

use AuthBundle\Command\RegisterUserCommand;
use AuthBundle\Entity\User;
use AuthBundle\Entity\UserToken;
use AuthBundle\Handler\RegisterUserHandler;
use AuthBundle\Repository\UserRepository;
use AuthBundle\Util\ObjectValidator;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @Route("/auth")
 */
class AuthController extends Controller
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var RegisterUserHandler
     */
    private $registerUserHandler;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    /**
     * @var ObjectValidator
     */
    private $validator;

    /**
     * AuthController constructor.
     * @param EntityManagerInterface $em
     * @param UserRepository $userRepository
     * @param RegisterUserHandler $registerUserHandler
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param ObjectValidator $validator
     */
    public function __construct(
        EntityManagerInterface $em,
        UserRepository $userRepository,
        RegisterUserHandler $registerUserHandler,
        UserPasswordEncoderInterface $passwordEncoder,
        ObjectValidator $validator
    ) {
        $this->em = $em;
        $this->userRepository = $userRepository;
        $this->registerUserHandler = $registerUserHandler;
        $this->passwordEncoder = $passwordEncoder;
        $this->validator = $validator;
    }

    /**
     * @Route("/register_user")
     * @Method({"POST"})
     */
    public function registerUser(Request $request)
    {
        $this->validator->validate($request->request->all(), $this->getRegistrationRules());

        $username = $request->get('username');
        $password = $request->get('password');
        $email = $request->get('email');

        $user = $this->registerUserHandler->handle(
            new RegisterUserCommand(
                $username,
                $email,
                $password
            )
        );

        return new View([
            'id' => $user->getId(),
            'username' => $user->getUsername()
        ], 201);

    }

    private function getRegistrationRules()
    {
        $constraint = new Assert\Collection([
            'username' => new Assert\Length(['min' => 6, 'max' => 24]),
            'password' => new Assert\Length(['min' => 6, 'max' => 100]),
            'email' => new Assert\Email(),
        ]);

        return $constraint;
    }

    /**
     * @Route("/create_token", name="auth_create_token")
     * @Method({"POST"})
     */
    public function createToken(Request $request)
    {
        $this->validator->validate($request->request->all(), $this->getCredentialsRules());

        $username = $request->get('username');
        $password = $request->get('password');

        /* @var User $user */
        $user = $this->userRepository->findOneByUsername($username);

        if (!$user || !$this->passwordEncoder->isPasswordValid($user, $password)) {
            throw new BadRequestHttpException('Wrong credentials');
        }

        $token = bin2hex(random_bytes(64));

        $userToken = new UserToken();
        $userToken->setExpires(new \DateTime('+24 hours'));
        $userToken->setToken($token);
        $userToken->setUser($user);

        $this->em->persist($userToken);
        $this->em->flush();

        return new View([
            'username' => $user->getUsername(),
            'access_token' => $userToken->getToken()
        ], 201);
    }

    private function getCredentialsRules()
    {
        $constraint = new Assert\Collection([
            'username' => new Assert\Length(['min' => 6, 'max' => 24]),
            'password' => new Assert\Length(['min' => 6, 'max' => 100]),
        ]);

        return $constraint;
    }
}
