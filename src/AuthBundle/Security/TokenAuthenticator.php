<?php

namespace AuthBundle\Security;

use AuthBundle\Entity\UserToken;
use AuthBundle\Repository\UserTokenRepository;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\TokenExtractor\AuthorizationHeaderTokenExtractor;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

class TokenAuthenticator extends AbstractGuardAuthenticator
{
    /**
     * @var UserTokenRepository
     */
    private $userTokenRepository;

    /**
     * @param UserTokenRepository $userTokenRepository
     */
    public function __construct(UserTokenRepository $userTokenRepository)
    {
        $this->userTokenRepository = $userTokenRepository;
    }

    /**
     * @inheritdoc
     */
    public function getCredentials(Request $request)
    {
        $headers = $request->headers;

        if (!$headers->has('Authorization')) {
            return null;
        }

        list($token) = sscanf(
            $headers->get('Authorization'),
            'Bearer %s'
        );

        if (!$token) {
            return null;
        }

        return $token;
    }

    /**
     * @inheritdoc
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        if (!$credentials) {
            throw new HttpException(401, 'Invalid access token');
        }

        /* @var UserToken $token */
        $token = $this->userTokenRepository->findOneByToken($credentials);

        if (!$token) {
            throw new HttpException(401, 'Invalid access token');
        }

        // todo: find a better place for validating the token
        if (!$token->isValid()) {
            throw new HttpException(401, 'Invalid access token (probably expired)');
        }

        return $token->getUser();
    }

    /**
     * @inheritdoc
     */
    public function checkCredentials($credentials, UserInterface $user)
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $data = [
            'message' => strtr($exception->getMessageKey(), $exception->getMessageData())
        ];

        return new JsonResponse($data, Response::HTTP_FORBIDDEN);
    }

    /**
     * @inheritdoc
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
    }

    /**
     * @inheritdoc
     */
    public function supportsRememberMe()
    {
        return false;
    }

    /**
     * @inheritdoc
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        return new JsonResponse(['message' => 'Token is missing!'], Response::HTTP_UNAUTHORIZED);
    }
}
